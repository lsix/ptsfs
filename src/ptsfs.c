#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifndef _GNU_SOURCE
#define _GNU_SOURCE 1
#endif

#include <hurd/netfs.h>
#include <assert.h>

#include "ptsfs.h"

#include "log.h"

error_t
ptsfs_root_get_contents (char **content,
                         size_t *content_len)
{
	// The strict minimum of pts content is
	// - '.'
	// - '..'
	// - 'ptmx'
	static const char ptsfs_root_dir_content[] = ".\0..\0ptmx";
	static size_t size_of_pts_fix_content = sizeof (ptsfs_root_dir_content);
	size_t num_of_nodes = 3; // we at least have ".", ".." and "ptmx"
	size_t size_of_contents = size_of_pts_fix_content;

	// Find out the total size of the buffer that will be used to return
	// the content of the directory
	struct ptsslave *n = (struct ptsslave *) netfs_root_node->nn->pn->hook;
	while (n) {
		num_of_nodes++;
		size_of_contents += n->name_len + 1;
		n = n->next;
	}

	// Allocate the buffer based on the needed size
	*content = (char *) malloc (size_of_contents);
	if (*content == NULL)
		return ENOMEM;
	
	// Go back at the begining of the list, and then construct the result
	// to return it
	n = (struct ptsslave *) netfs_root_node->nn->pn->hook;
	char *content_ptr = *content;
	memcpy (content_ptr, ptsfs_root_dir_content, size_of_pts_fix_content);
	content_ptr += size_of_pts_fix_content;
	
	while (n) {
		assert (strlen (n->name) == n->name_len);
		strncpy (content_ptr, n->name, n->name_len);
		content_ptr += n->name_len + 1;
		n = n->next;
	}
	*content_len = size_of_contents;

	assert (*content + *content_len == content_ptr);
	return 0;
}

error_t
ptsfs_new_pts_slave ()
{
	struct ptsslave *sl = (struct ptsslave *) malloc (sizeof (struct ptsslave));

	if (sl == NULL)
		return ENOMEM;

	// Create a new node
	sl->id = next_id++;
	int tmp = asprintf (&(sl->name), "%d", (int) sl->id);
	if (tmp == -1) {
		free (sl);
		return ENOMEM;
	}
	sl->name_len = strlen (sl->name);

	// insert the new node in the linked list
	sl->next = (struct ptsslave *) netfs_root_node->nn->pn->hook;
	netfs_root_node->nn->pn->hook = (void *) sl;

	return 0;
}

error_t
ptfs_delete_pts_slave (size_t id)
{
	struct ptsslave *curr = (struct ptsslave *) netfs_root_node->nn->pn->hook;
	struct ptsslave *pred = curr;

	if (curr == NULL)
	{
		return 0;
	}
	else if (curr->id = id)
	{
		netfs_root_node->nn->pn->hook = curr->next;
		free (curr);
		return 0;
	}
	else
	{
		do {
			pred = curr;
			curr = curr->next;
		} while (curr != NULL  && curr->id != id);

		pred->next = curr->next;
		free (curr);
		return 0;
	}
}

struct node *
ptsfs_make_root_node ()
{
	assert (next_id == 1);
	struct ptsfsnode *rootnode = malloc (sizeof (struct ptsfsnode));
	if (rootnode == NULL)
		return NULL;

	rootnode->hook = NULL;
	rootnode->get_contents = ptsfs_root_get_contents;

	struct netnode *netnode = malloc (sizeof (struct netnode));
	if (netnode == NULL) {
		free (rootnode);
		return NULL;
	}
	netnode->pn = rootnode;

	struct node *n = netfs_make_node (netnode);
	if (n == NULL) {
		free (rootnode);
		free (netnode);
	}

	assert (n->nn == netnode);
	return n;
}
