#ifndef PTSFS_H
#define PTSFS_H

#include <hurd/netfs.h>

/// TODO remove this global (is it even usefull ?)
static size_t next_id = 1;

struct ptsslave
{
	struct ptsslave *next;

	size_t id;	

	// caching for the name
	size_t name_len;
	char *name;
};

struct ptsfsnode
{

	/// Returns the content of the current node
	error_t (*get_contents) (char **content
                                ,size_t *content_len);

	/// Any node can use this hook for internal use.
	/// Root node uses it to store the list of associated
	/// pts nodes (link to the first element of the list)
	void *hook;
};

struct netnode
{
	struct ptsfsnode * pn;
};

/// Callbacks for the root node
error_t ptsfs_root_get_contents (char **content
                                ,size_t *content_len);
error_t ptsfs_new_pts_slave ();
error_t ptsfs_delete_pts_slave (size_t id);

struct node * ptsfs_make_root_node ();

#endif
