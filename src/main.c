#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifndef _GNU_SOURCE
#define _GNU_SOURCE 1
#endif

#include "log.h"
#include "ptsfs.h"

#include <unistd.h>
#include <error.h>
#include <stdio.h>
#include <string.h>

#include <mach.h>
#include <hurd.h>

#include <hurd/netfs.h>

char *netfs_server_name = PACKAGE_NAME;
char *netfs_server_version = PACKAGE_VERSION;

int netfs_maxsymlinks = 1;

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//         \ | | | | | | | | | | | | | | | | | | | | | | | | | | /            //
//        - Most of the implementation is located in ptsfs.c file -           //
//         / | | | | | | | | | | | | | | | | | | | | | | | | | | \            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

int
main(int argc,
     char **argv)
{
	io_statbuf_t underlying_stat;
	char buff[400];
	log_init();
        mach_port_t bootstrap, underlying_node;

	// TODO parse arguments
	//
	_log ("Bootstraping");
        task_get_bootstrap_port (mach_task_self (),
                                 &bootstrap);

	_log ("init libnetfs");
	netfs_init ();

	_log ("Starting up libnetfs");
	underlying_node = netfs_startup (bootstrap, 0);

	if (io_stat (underlying_node, &underlying_stat))
		error (1, 0, "Unable to grab underlying node's stat");

	netfs_root_node = ptsfs_make_root_node ();
	netfs_root_node->nn_stat = underlying_stat;

	netfs_root_node->nn_stat.st_mode = S_IFDIR | S_IRUSR | S_IXUSR |
		                                     S_IRGRP | S_IXGRP |
						     S_IROTH | S_IXOTH;
	// the root node has mode 0666
	netfs_root_node->owner = netfs_root_node->nn_stat.st_uid;

	_log ("launch server");
	for (;;) {
		netfs_server_loop ();
		_log ("Out of loop");
	}
	log_end();
        return 0;
}
