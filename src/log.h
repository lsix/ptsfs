#ifndef LOG_H
#define LOG_H

#include <stdio.h>

void
log_init();

FILE*
get_log ();

void
_log(char const * const);

void
log_end();

#endif
