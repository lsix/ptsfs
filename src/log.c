#include "log.h"

FILE *f;

void
log_init()
{
	f = fopen ("/home/lsix/ptslog", "w");
}

FILE*
get_log ()
{
	return f;
}

void
_log(char const * const m)
{
	fprintf (f, "%s\n", m);
	fflush (f);
}

void
log_end()
{
	fclose (f);
}
